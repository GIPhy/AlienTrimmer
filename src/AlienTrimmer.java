/*
  ########################################################################################################

  AlienTrimmer:  a tool to quickly and accurately trim off multiple short contaminant sequences from high-
  throughput sequencing reads 
    
  Copyright (C) 2013-2020  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contacts:

   Institut Pasteur, Paris, FRANCE                                      (BURL=)https://research.pasteur.fr

   + Sylvain Brisse                                                              sylvain.brisse@pasteur.fr
   Unit BEBP                               $BURL/team/biodiversity-and-epidemiology-of-bacterial-pathogens
   NRC 'Whooping Cough and other Bordetella infections'   $BURL/nrc/whooping-cough-and-other-bordetelloses
   NRC 'Corynebacteria of the diphtheriae complex'     $BURL/nrc/corynebacteriae-of-the-diphteriae-complex
   Dpt. Global Health                                                       $BURL/department/global-health

   + Alexis Criscuolo                                                          alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                               $BURL/team/hub-giphy
   USR 3756 IP CNRS                                        $BURL/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                                   $BURL/department/computational-biology

  ########################################################################################################
*/
/*
  ########################################################################################################
  ## v2.1.201124ac
  +  fixed bug: no. reads and bases are now stored using long primitives

  ## v2.0.201111ac
  +  simplified options
  +  ability to read/writer fastq.gz files
  +  ability to be compiled using GraalVM native-image

  ########################################################################################################
*/


/*
  ########################################################################################################
  ### TECHNICAL NOTES                                                                                  ###
  ########################################################################################################

  ### Nucleotide encoding ############################################

  base  2-bit-encoding 4-bit-encoding
  ---- -------------- --------------
  A    0 (00)         1 (0001)
  C    1 (01)         2 (0010)
  G    2 (10)         4 (0100)
  T    3 (11)         8 (1000)


  ### Bitset implementation with fixed length based on primitives ####

  java.util.BitSet                 long array
  ----------------                 ----------
  BitSet bs = new BitSet(lgt)      long bs = new long[(lgt+63)>>6];
  bs.get(i)                        (bs[i>>6] & (1L << i)) != 0
  bs.set(i)                        bs[i>>6] |= (1L << i)

  Of note, i>>6 is equivalent to i/64.
  For more details, see e.g.:
  http://developer.classpath.org/doc/java/util/BitSet-source.html
  https://android.googlesource.com/platform/libcore/+/jb-mr2-release/luni/src/main/java/java/util/BitSet.java
  https://android.googlesource.com/platform/libcore.git/+/android-4.2.2_r1/luni/src/main/java/java/util/BitSet.java


  ### Trimming criteria ##############################################

  ## 3' 
  read:         ....ACGTACTACGTACGTAGTACGTACGT
  troublesome:       *      *  ************
  trimming:                 <<<<<<<<<<<<<<<<<<
                            |             |   |
                            e             t   l

  t: index of the last troublesome base
  e: index <= t such that base e is troublesome
  l: read length

  AlienTrimmer trims the longest suffix [e,l-1] such that:
   + the proportion x of troublesome bases in [e,l-1] is > 50%
   + no more than m successive non-troublesome bases occur in [e,t] (option -m; default: 6)

  ## 5'  
  read:         ACGTACTACGTACGTAGTACGTACGT....
  troublesome:     * ****** *  *      *
  trimming:     >>>>>>>>>>>>>>>>
                   |            |
                   t            s

  t: index of the first troublesome base
  s: index >= t (and < e) such that base s-1 is troublesome

  AlienTrimmer trims the longest prefix [0,s-1] such that:
   + the proportion x of troublesome bases in [0,s-1] is > 50%
   + no more than m successive non-troublesome bases occur in [t,s-1] (option -m; default: 6)

  ########################################################################################################
*/

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class AlienTrimmer {

    //### constants  ################################################################
    final static String VERSION = "2.1.201124ac   Copyright (C) 2013-2020  Institut Pasteur";
    final static String NOTHING = "N.o./.T.h.I.n.G";
    static final int BUFFER_SIZE = 65536; //## NOTE: buffer size for gzip streams
    static final byte B_1 = (byte) -1;    static final byte B0  = (byte)  0;
    static final byte B1  = (byte)  1;    static final byte B2  = (byte)  2;
    static final byte B3  = (byte)  3;    static final byte B4  = (byte)  4;
    static final byte B5  = (byte)  5;    static final byte B6  = (byte)  6;
    static final byte B8  = (byte)  8;    static final byte B10 = (byte) 10;
    static final byte B15 = (byte) 15;    static final byte B64 = (byte) 64;

    //### options   #################################################################
    static File infq1;       // -i/-1   (r1) fastq input file (mandatory)
    static File infq2;       // -2      (r2) fastq input file
    static File alien1;      // -a --a1 (r1) alien oligo txt file (mandatory)
    static File alien2;      // --a2    (r2) alien oligo txt file
    static String basename;  // -o      basename for output file (mandatory)
    static int phred;        // --p64   Phred+64 if set (default: Phred+33)
    static byte k;           // -k      k-mer size (default: 10)
    static char qmin;        // -q      minimum allowed Phred Q score (default: 13)
    static byte m;           // -m      maximum consecutive non-troublesome bases in trimmed regions (default: k-1)
    static int minLgt;       // -l      min read length to output
    static boolean gz;       // -z      gzipped output files
    static boolean verbose;  // -v      verbose mode
    static int prop;         // -p      minimum percentage of correct nucleotides (i.e. >= qmin)
    
    //### io   ######################################################################
    static BufferedReader in1, in2;
    static BufferedWriter out1, out2, outS;

    //### data   ####################################################################
    static long t;             // starting date in milliseconds
    static boolean paired;     // true when paired read data
    static int imsk;           //  int mask := 2^k - 1
    static long lmsk;          // long mask := 4^k - 1
    static byte k_1;           // k-1
    static long[] bk1;         // bitset to store 2-bit-encoded alien1 k-mers
    static long[] lk1;         // long array to store 4-bit-encoded alien1 k-mers
    static long[] bk2;         // bitset to store 2-bit-encoded alien2 k-mers
    static long[] lk2;         // long array to store 4-bit-encoded alien2 k-mers
    static String l1_1, l2_1;  // line 1 of FASTQ block
    static String l1_2, l2_2;  // line 2 of FASTQ block
    static String l1_3, l2_3;  // line 3 of FASTQ block
    static String l1_4, l2_4;  // line 4 of FASTQ block
    static int lgt1, lgt2;     // length of FASTQ block line 2 (== line 4 length)
    static byte[] cov;         // covering k-mer scores
    static byte[] bad;         // low phred scores
    static int s1, e1, s2, e2; // substring(s,e) to trim read
    static boolean discard1;   // true when R1 is discarded
    static boolean discard2;   // true when R2 is discarded
    static long cnt_r_in1;     // no. reads in R1 input file
    static long cnt_r_in2;     // no. reads in R2 input file
    static long cnt_b_in1;     // no. bases in R1 input file
    static long cnt_b_in2;     // no. bases in R2 input file
    static long cnt_r_out1;    // no. reads in R1 output file
    static long cnt_r_out2;    // no. reads in R2 output file
    static long cnt_r_outS;    // no. reads in R2 output file
    static long cnt_b_out1;    // no. bases in R1 output file
    static long cnt_b_out2;    // no. bases in R2 output file
    static long cnt_b_outS;    // no. bases in R2 output file

    //### stuffs   ##################################################################
    static byte s, n;
    static int i, j, l, o, q, x, y;
    static int iptn;
    static long lptn, nptn, lkmer;
    static char ch;
    static boolean noN, noTrim, match;
    static String oligo, filename;
    static char[] ca, cp;
    static ArrayList<String> als;
    static TreeSet<Long> tsl;

    
    public static void main(String[] args) throws IOException {

	//#############################################################################################################
	//### doc                                                                                                   ###
	//#############################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" AlienTrimmer v." + VERSION);
	    System.out.println("");
	    System.out.println(" Fast trimming to  filter out  non-confident  nucleotides and alien  oligo-nucleotide sequences");
	    System.out.println(" (adapters, primers) in both 5' and 3' read ends");
	    System.out.println(" Criscuolo and Brisse (2013) doi:10.1016/j.ygeno.2013.07.011");
	    System.out.println("");
	    System.out.println(" USAGE:  AlienTrimmer  [options]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("    -i <infile>   [SE] FASTQ-formatted input file; filename should end with .gz when gzipped");
	    System.out.println("    -1 <infile>   [PE] FASTQ-formatted R1 input file; filename should end with .gz when gzipped");
	    System.out.println("    -2 <infile>   [PE] FASTQ-formatted R2 input file; filename should end with .gz when gzipped");
	    System.out.println("    -a <infile>   [SE/PE] input file name containing alien sequence(s);  one line per sequence;");
	    System.out.println("                  lines starting with '>', '%' or '#' are not considered");
	    System.out.println("  --a1 <infile>   [PE] same as -a for only R1 reads");
	    System.out.println("  --a2 <infile>   [PE] same as -a for only R2 reads");
	    System.out.println("    -o <name>     outfile basename: [SE] <name>.fastq[.gz]  or  [PE] <name>.{1,2,S}.fastq[.gz];");
	    System.out.println("                  .gz is added when using option -z");
	    System.out.println("    -k [5-15]     k-mer length k for alien sequence occurence searching; must lie between 5 and");
	    System.out.println("                  15 (default: 10)");
	    System.out.println("    -q [0-40]     Phred quality score  cutoff to define  low-quality bases;  must lie between 0");
	    System.out.println("                  and 40 (default: 13)");
	    System.out.println("    -m <int>      maximum no. allowed successive  non-troublesome bases in the 5'/3' regions to");
	    System.out.println("                  be trimmed (default: k-1)");
	    System.out.println("    -l <int>      minimum allowed read length (default: 50)");
	    System.out.println("    -p [0-100]    maximum allowed percentage of low-quality bases per read (default: 50)");
	    System.out.println(" --p64            Phred+64 FASTQ input file(s) (default: Phred+33)");
	    System.out.println("    -z            gzipped output files (default: not set)");
	    System.out.println("    -v            verbose mode (default: not set)");
	    System.out.println("");
	    System.out.println(" EXAMPLES:");
	    System.out.println("  [SE]  AlienTrimmer -i reads.fq -c aliens.fa -o trim -l 30 -p 20");
	    System.out.println("  [SE]  AlienTrimmer -i reads.fq.gz -c aliens.txt -o trim -k 9 -q 13");
	    System.out.println("  [PE]  AlienTrimmer -1 r1.fq -2 r2.fq -c aliens.fa -o trim -m 8 -p 25 -v");
	    System.out.println("  [PE]  AlienTrimmer -1 r2.fq.gz -2 r2.fq.gz --a1 fwd.fa --a2 rev.fa -o trim -z");
	    System.out.println("");
	    System.exit(0);
	}

	
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	paired = false;
	infq1  = new File(NOTHING); // -1
	infq2  = new File(NOTHING); // -2
	alien1 = new File(NOTHING); // -a --a1
	alien2 = new File(NOTHING); // --a2
	basename = NOTHING;         // -o
	phred = 33;                 // --p64
	k = B10;                    // -k
	m = B_1;                    // -m
	minLgt = 50;                // -l
	q = 13;                     // -q
	prop = 50;                  // -p
	gz = false;                 // -z
	verbose = false;            // -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     { infq1 = new File(args[++o]);                continue; }
	    if ( args[o].equals("-1") )     { infq1 = new File(args[++o]);                continue; }
	    if ( args[o].equals("-2") )     { infq2 = new File(args[++o]); paired = true; continue; }
	    if ( args[o].equals("-a") )     { alien1 = new File(args[++o]);               continue; }
	    if ( args[o].equals("--a1") )   { alien1 = new File(args[++o]);               continue; }
	    if ( args[o].equals("--a2") )   { alien2 = new File(args[++o]);               continue; }
	    if ( args[o].equals("-o") )     { basename = args[++o];                       continue; }
	    if ( args[o].equals("-k") ) try { k = Byte.parseByte(args[++o]);              continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -k)"); System.exit(1); }
	    if ( args[o].equals("-m") ) try { m = Byte.parseByte(args[++o]);              continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -m)"); System.exit(1); }
	    if ( args[o].equals("-q") ) try { q = Integer.parseInt(args[++o]);            continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -q)"); System.exit(1); }
	    if ( args[o].equals("-l") ) try { minLgt = Integer.parseInt(args[++o]);       continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -l)"); System.exit(1); }
	    if ( args[o].equals("-p") ) try { prop = Integer.parseInt(args[++o]);         continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -l)"); System.exit(1); }
	    if ( args[o].equals("--p64") )  { phred = 64;                                 continue; }
	    if ( args[o].equals("-z") )     { gz = true;                                  continue; }
	    if ( args[o].equals("-v") )     { verbose = true;                             continue; }
	}
	//### assessing options
	if ( infq1.toString().equals(NOTHING) )  { System.err.println("no input file (option -1)");                                             System.exit(1); }
	if ( ! infq1.exists() )                  { System.err.println("file " + infq1.toString() + " does not exist (option -1)");              System.exit(1); }
	if ( alien1.toString().equals(NOTHING) ) { System.err.println("no alien file (option -a)");                                             System.exit(1); }
	if ( ! alien1.exists() )                 { System.err.println("file " + alien1.toString() + " does not exist (option -a)");             System.exit(1); }
	if ( basename.equals(NOTHING) )          { System.err.println("no output file basename (option -o)");                                   System.exit(1); }
	if ( paired ) {
	    if ( ! infq2.exists() )              { System.err.println("file " + infq2.toString() + " does not exist (option -2)");              System.exit(1); }
	    if ( alien2.toString().equals(NOTHING) ) alien2 = new File(alien1.toString());
	    else if ( ! alien2.exists() )        { System.err.println("file " + alien2.toString() + " does not exist (option --a2)");           System.exit(1); }
	}
	if ( prop < 0 || prop > 100 )            { System.err.println("percentage should be set between 0 and 100 (option -p)");                System.exit(1); }
	if ( q < 0 || q > 40 )                   { System.err.println("Phred quality score cutoff should be set between 0 and 40 (option -q)"); System.exit(1); }
	if ( k < B5 || k > B15 )                 { System.err.println("k-mer length should be set between 5 and 15 (option -k)");               System.exit(1); }
	if ( m < B_1 || m > B64 )                { System.err.println("option -m should be set between 0 and 64");                              System.exit(1); }
	if ( minLgt <= k )                       { System.err.println("minimum read length should be higher than k-mer length (option -l)");    System.exit(1); }
	

	//#############################################################################################################
	//### init variables                                                                                        ###
	//#############################################################################################################
	t = System.currentTimeMillis();
	qmin = (char) (phred+q);           //## NOTE: char corresponding to min Phred Q score
	imsk = 1;  imsk <<= B2*k; --imsk;  //## NOTE: 2-bit mask
	lmsk = 1L; lmsk <<= B4*k; --lmsk;  //## NOTE: 4-bit mask
	bk1 = new long[(imsk+64)>>6];      //## NOTE: alien1 bitset; new BitSet(imsk+1);
	k_1 = k; --k_1;
	if ( m == B_1 ) m = k_1;           //## NOTE: maximum no. allowed successive non-troublesome bases (default = k-1)
	cov = new byte[256];               //## NOTE: covering k-mer scores
	bad = new byte[256];               //## NOTE: low phred scores
	//## file readers/writers
	filename = infq1.toString();
	in1     = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER_SIZE))) : Files.newBufferedReader(Path.of(filename));
	if ( paired ) {
	    filename = infq2.toString();
	    in2 = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER_SIZE))) : Files.newBufferedReader(Path.of(filename));
	    filename = basename + ".1.fastq";
	    out1 = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER_SIZE)), BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    filename = basename + ".2.fastq";
	    out2 = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER_SIZE)), BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    filename = basename + ".S.fastq";
	    outS = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER_SIZE)), BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	}
	else {
	    filename = basename + ".fastq";
	    out1 = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER_SIZE)), BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	}
	//## counters
	cnt_r_in1  = cnt_r_in2  = cnt_b_in1  = cnt_b_in2  = 0;
	cnt_r_out1 = cnt_r_out2 = cnt_r_outS = cnt_b_out1 = cnt_b_out2 = cnt_b_outS = 0;
	
	
	//#############################################################################################################
	//### displaying summary                                                                                    ###
	//#############################################################################################################
	System.out.println("");
	System.out.println("AlienTrimmer v" + VERSION);
	System.out.println("");
	if ( ! paired ) System.out.println("FASTQ file:         " + infq1.toString());
	else            System.out.println("FASTQ files:        " + infq1.toString() + "  " + infq2.toString());
	System.out.println("main options:       -k " + k + "  -m " + m + "  -q " + q + "  -l " + minLgt + "  -p " + prop + ((phred==64)?"  --p64":"")   );
	if ( ! paired ) System.out.println("outfile:            " + ((gz)?basename+".fastq.gz":basename+".fastq"));
	else            System.out.println("outfiles:           " + basename + ((gz)?".1.fastq.gz":".1.fastq") + "  " + basename + ((gz)?".2.fastq.gz":".2.fastq") + "  " + basename + ((gz)?".S.fastq.gz":".S.fastq"));
    

	//#############################################################################################################
	//### preprocessing alien oligos                                                                            ###
	//#############################################################################################################
	
	//## reading alien1 oligos  ############################################################
	als = getAlienOligo(alien1);
	if ( als.size() == 0 ) { System.err.println("no alien oligo  (option -a / --a1)"); System.exit(1); }

	//## k-mering alien1 oligos  ###########################################################
	tsl = new TreeSet<Long>();
	o = als.size();
	while ( --o >= 0 ) {
	    if ( (l=(ca=als.get(o).toCharArray()).length) < k ) continue;  //## NOTE: current oligo too short, i.e. < k
	    iptn = B0; lptn = 0; j = B_1;
	    while ( ++j < k_1 ) { 
		iptn <<= B2; lptn <<= B4; switch ( ca[j] ) { case 'A': lptn |= B1; continue; case 'C': iptn |= B1; lptn |= B2; continue; case 'G': iptn |= B2; lptn |= B4; continue; case 'T': iptn |= B3; lptn |= B8; continue; }
	    }
	    --j;
	    while ( ++j < l ) {
		iptn <<= B2; lptn <<= B4; switch ( ca[j] ) { case 'A': lptn |= B1; break; case 'C': iptn |= B1; lptn |= B2; break; case 'G': iptn |= B2; lptn |= B4; break; case 'T': iptn |= B3; lptn |= B8; break; }
		iptn &= imsk; bk1[iptn >> 6] |= (1L << iptn);  //## NOTE: storing 2-bit-encoded k-mer ending at position j;  BitSet.set( iptn &= imsk )
		tsl.add((lptn &= lmsk));                       //## NOTE: storing 4-bit-encoded k-mer ending at position j
		// System.out.println(als.get(o).substring(j-k+1,j+1) + "  " + Integer.toBinaryString(iptn) + "  " + Long.toBinaryString(lptn));
	    }
	}
	lk1 = new long[tsl.size()]; o = B_1; for (long fel: tsl) lk1[++o] = fel;  //## NOTE: array containing sorted 4-bit-encoded alien1 k-mers
	
	if ( paired ) {
	    if ( alien1.toString().equals(alien2.toString()) ) {  //## NOTE: same alien oligo file for r1 and r2
		bk2 = Arrays.copyOf(bk1, bk1.length);
		lk2 = Arrays.copyOf(lk1, lk1.length);
	    }
	    else {
		//## reading alien2 oligos  ########################################################
		als = getAlienOligo(alien2);
		if ( als.size() == 0 ) { System.err.println("no alien oligo  (option --a2)"); System.exit(1); }

		//## k-mering alien2 oligos  #######################################################
		bk2 = new long[(imsk+64)>>6]; //## NOTE: new BitSet(imsk+1);
		tsl.clear();
		o = als.size();
		while ( --o >= 0 ) {
		    if ( (l=(ca=als.get(o).toCharArray()).length) < k ) continue;  //## NOTE: current oligo too short, i.e. < k
		    iptn = B0; lptn = 0; j = B_1;
		    while ( ++j < k_1 ) { 
			iptn <<= B2; lptn <<= B4; switch ( ca[j] ) { case 'A': lptn |= B1; continue; case 'C': iptn |= B1; lptn |= B2; continue; case 'G': iptn |= B2; lptn |= B4; continue; case 'T': iptn |= B3; lptn |= B8; continue; }
		    }
		    --j;
		    while ( ++j < l ) {
			iptn <<= B2; lptn <<= B4; switch ( ca[j] ) { case 'A': lptn |= B1; break; case 'C': iptn |= B1; lptn |= B2; break; case 'G': iptn |= B2; lptn |= B4; break; case 'T': iptn |= B3; lptn |= B8; break; }
			iptn &= imsk; bk2[iptn >> 6] |= (1L << iptn);  //## NOTE: storing 2-bit-encoded k-mer ending at position j;  BitSet.set( iptn &= imsk )
			tsl.add((lptn &= lmsk));                       //## NOTE: storing 4-bit-encoded k-mer ending at position j
		    }
		}
		lk2 = new long[tsl.size()]; o = B_1; for (long fel: tsl) lk2[++o] = fel;  //## NOTE: array containing sorted 4-bit-encoded alien2 k-mers
	    }

	    System.out.println("no. alien k-mers:   " + lk1.length + " (R1)  " + lk2.length + " (R2)");
	}
	else if ( verbose ) System.out.println("no. alien k-mers:   " + lk1.length);

	als = null;
	tsl = null;
	
	    
	//#############################################################################################################
	//### trimming/clipping                                                                                     ###
	//#############################################################################################################
	
	//### IMPORTANT  ###########################################################################
	//#   it is expected that no blank space occurs in line 2 and 4 of FASTQ blocks (i.e. sequenced read and associated Phred scores, respectively)
	//#   it is also expected that no empty line occurs in the FASTQ files
	//#   when paired, it is trivially expected that the two input files contain the same no. FASTQ blocks
	//##########################################################################################
	
	while ( (l1_1 = in1.readLine()) != null ) {
	    l1_2 = in1.readLine();  //## NOTE: R1 sequenced read
	    l1_3 = in1.readLine(); 
	    l1_4 = in1.readLine();  //## NOTE: R1 Phred scores

	    s1 = 0; e1 = lgt1 = l1_2.length(); 
	    ++cnt_r_in1; cnt_b_in1 += lgt1;
	    discard1 = false;
	    
	    if ( lgt1 < minLgt ) discard1 = true;
	    else {
		//## traversing R1  ################################################################
		//## k-mer decomposition, using algorithm S1.2 from https://ars.els-cdn.com/content/image/1-s2.0-S0888754313001481-mmc1.pdf
		ca = l1_2.toCharArray(); 
		cp = l1_4.toCharArray();
		if ( lgt1 >= cov.length ) { cov = new byte[++lgt1]; bad = new byte[lgt1]; --lgt1; }
		//Arrays.fill(cov, B0);
		
		noN = true;    //## NOTE: noN = true when there is no degenerated base
		noTrim = true; //## NOTE: noTrim = true when there is no troublesome base
		i = lgt1; cov[i] = B0;
		while ( --i >= 0 ) {
		    cov[i] = B0;
		    if ( cp[i] < qmin ) { noTrim = false; bad[i] = B1; } else bad[i] = B0;
		    switch ( ca[i] ) {
		    case 'N':              noN = false; continue;   case 'a': ca[i] = 'A';              continue;
		    case 'c': ca[i] = 'C';              continue;   case 'g': ca[i] = 'G';              continue;
		    case 't': ca[i] = 'T';              continue;   case 'n': ca[i] = 'N'; noN = false; continue;
		    }
		}

		if ( noN ) {  //## NOTE: no degenerated base
		    iptn = B0; i = j = B_1; s = B0;
		    while ( ++j < k_1 ) { 
			iptn <<= B2; switch ( ca[j] ) { case 'C': iptn |= B1; continue; case 'G': iptn |= B2; continue; case 'T': iptn |= B3; continue; } 
		    }
		    while ( j < lgt1 ) {
			iptn <<= B2; switch ( ca[j] ) { case 'C': iptn |= B1; break;    case 'G': iptn |= B2; break;    case 'T': iptn |= B3; break; }  iptn &= imsk;
			++j;
			
			cov[++i] += s;
			if ( (bk1[iptn >> 6] & (1L << iptn)) != 0L ) {  //## NOTE: if BitSet.get( iptn ) then match! 
			    ++cov[i]; --cov[j]; noTrim = false;
			}
			else if ( cov[i] < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		    while ( ++i < lgt1 ) {
			if ( (cov[i]+=s) < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		}
		else {   //## NOTE: at least one degenerated base
		    lptn = nptn = iptn = n = B0; i = j = B_1; s = B0;
		    while ( ++j < k_1 ) { 
			iptn <<= B2; lptn <<= B4; nptn <<= B4; 
			switch ( ca[j] ) {
			case 'A':             lptn |= B1; nptn |= B1;  break;   case 'C': iptn |= B1; lptn |= B2; nptn |= B2;  break; 
			case 'G': iptn |= B2; lptn |= B4; nptn |= B4;  break;   case 'T': iptn |= B3; lptn |= B8; nptn |= B8;  break; 
			default:  n = k;                  nptn |= B15; break; 
			} 
			--n;
		    }
		    while ( j < lgt1 ) {
			iptn <<= B2; lptn <<= B4; nptn <<= B4; 
			switch ( ca[j] ) {
			case 'A':             lptn |= B1; nptn |= B1;  break;   case 'C': iptn |= B1; lptn |= B2; nptn |= B2;  break; 
			case 'G': iptn |= B2; lptn |= B4; nptn |= B4;  break;   case 'T': iptn |= B3; lptn |= B8; nptn |= B8;  break; 
			default:  n = k;                  nptn |= B15; break; 
			} 
			--n;
			++j;
			
			match = false;
			if ( n < B0 ) { //## NOTE: no degenerated base in the current k-mer
			    iptn &= imsk; match = ( (bk1[iptn >> 6] & (1L << iptn)) != 0L );  //## NOTE: BitSet.get( iptn )
			}
			else {          //## NOTE: at least one degenerated base in the current k-mer
			    x = Arrays.binarySearch(lk1, (lptn &= lmsk)); x = -(++x); --x;
			    y = Arrays.binarySearch(lk1, (nptn &= lmsk)); y = -(++y); 
			    while ( ++x < y  &&  ((lkmer=lk1[x]) != (lkmer |= lptn)) ) {}
			    match = ( x != y );
			}
			
			cov[++i] += s;
			if ( match ) { ++cov[i]; --cov[j];  noTrim = false; }
			else if ( cov[i] < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		    while ( ++i < lgt1 ) {
			if ( (cov[i]+=s) < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		}

		if ( ! noTrim ) {
		    //## trimming/clipping 3' R1  ######################################################
		    i = lgt1; x = 0;                        //## NOTE: x >= 0 when suffix [i,lgt1[ contains > 50% troublesome bases
		    while ( --i >= 0 && bad[i] == B0 && cov[i] == B0 ) --x;
		    if ( i >= 0 ) {
			e1 = ( ++x >= 0 ) ? i : e1; s = m;  //## NOTE: s = 0 when m successive non-troublesome bases in suffix [i,t] (see Technical Notes)
			while ( --i >= 0 && s > B0 ) if ( bad[i] > B0 || cov[i] > B0 ) { e1 = ( ++x >= 0 ) ? i : e1; s = m; } else { --x; --s; }
		    }
		    //## trimming/clipping 5' R1  ######################################################
		    s1 = i = B_1; x = 0;
		    while ( ++i < e1 && bad[i] == B0 && cov[i] == B0 ) --x;
		    if ( i < e1 ) {
			s1 = ( ++x >= 0 ) ? i : s1; s = m;
			while ( ++i < e1 && s > B0 ) if ( bad[i] > B0 || cov[i] > B0 ) { s1 = ( ++x >= 0 ) ? i : s1; s = m; } else { --x; --s; }
		    }
		    ++s1;
		    //## assessing R1 length  ##########################################################
		    l = e1 - s1;                  //## NOTE: l = length of trimmed read
		    discard1 = ( l < minLgt );
		    //## assessing R1 composition  #####################################################
		    if ( ! discard1 ) {
			i = e1; x = 0; while ( --i >= s1 ) x += bad[i];
			discard1 = ( 100 * x > l * prop );  //## NOTE: x/l > prop/100, where x is the no. 'bad' bases in the trimmed read
		    }
		    
		    if ( verbose && l != lgt1 ) display(l1_1, l1_2, l1_4, cov, bad, lgt1, s1, e1, discard1);
		}
	    }

	    //## writing single-end outfile  #######################################################
	    if ( ! paired ) {
		if ( ! discard1 ) {
		    l = e1 - s1;
		    ++cnt_r_out1; cnt_b_out1 += l;
		    out1.write(l1_1);                                             out1.newLine();
		    out1.write( ((l != lgt1) ? l1_2.substring(s1, e1) : l1_2 ) ); out1.newLine();
		    out1.write(l1_3);                                             out1.newLine();
		    out1.write( ((l != lgt1) ? l1_4.substring(s1, e1) : l1_4 ) ); out1.newLine();
		}
		continue;
	    }

	    //######################################################################################
	    //## dealing with R2 (if any) ##########################################################
	    //######################################################################################
	    l2_1 = in2.readLine();
	    l2_2 = in2.readLine();  //## NOTE: R2 sequenced read
	    l2_3 = in2.readLine(); 
	    l2_4 = in2.readLine();  //## NOTE: R2 Phred scores

	    s2 = 0; e2 = lgt2 = l2_2.length(); 
	    ++cnt_r_in2; cnt_b_in2 += lgt2;
	    discard2 = false;
	    
	    if ( lgt2 < minLgt ) discard2 = true;
	    else {
		//## traversing R2  ################################################################
		//## k-mer decomposition, using algorithm S1.2 from https://ars.els-cdn.com/content/image/1-s2.0-S0888754313001481-mmc1.pdf
		ca = l2_2.toCharArray(); 
		cp = l2_4.toCharArray();
		if ( lgt2 >= cov.length ) { cov = new byte[++lgt2]; bad = new byte[lgt2]; --lgt2; }
		//Arrays.fill(cov, B0);
		
		noN = true;    //## NOTE: noN = true when there is no degenerated base
		noTrim = true; //## NOTE: noTrim = true when there is no troublesome base
		i = lgt2; cov[i] = B0;
		while ( --i >= 0 ) {
		    cov[i] = B0;
		    if ( cp[i] < qmin ) { noTrim = false; bad[i] = B1; } else bad[i] = B0;
		    switch ( ca[i] ) {
		    case 'N':              noN = false; continue;   case 'a': ca[i] = 'A';              continue;
		    case 'c': ca[i] = 'C';              continue;   case 'g': ca[i] = 'G';              continue;
		    case 't': ca[i] = 'T';              continue;   case 'n': ca[i] = 'N'; noN = false; continue;
		    }
		}
		
		if ( noN ) {  //## NOTE: no degenerated base
		    iptn = B0; i = j = B_1; s = B0;
		    while ( ++j < k_1 ) { 
			iptn <<= B2; switch ( ca[j] ) { case 'C': iptn |= B1; continue; case 'G': iptn |= B2; continue; case 'T': iptn |= B3; continue; } 
		    }
		    while ( j < lgt2 ) {
			iptn <<= B2; switch ( ca[j] ) { case 'C': iptn |= B1; break;    case 'G': iptn |= B2; break;    case 'T': iptn |= B3; break; }  iptn &= imsk;
			++j;
			
			cov[++i] += s;
			if ( (bk2[iptn >> 6] & (1L << iptn)) != 0L ) {  //## NOTE: if BitSet.get( iptn ) then match!
			    ++cov[i]; --cov[j]; noTrim = false;
			}
			else if ( cov[i] < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		    while ( ++i < lgt2 ) {
			if ( (cov[i]+=s) < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		}
		else {   //## NOTE: at least one degenerated base
		    lptn = nptn = iptn = n = B0; i = j = B_1; s = B0;
		    while ( ++j < k_1 ) { 
			iptn <<= B2; lptn <<= B4; nptn <<= B4; 
			switch ( ca[j] ) {
			case 'A':             lptn |= B1; nptn |= B1;  break;   case 'C': iptn |= B1; lptn |= B2; nptn |= B2;  break; 
			case 'G': iptn |= B2; lptn |= B4; nptn |= B4;  break;   case 'T': iptn |= B3; lptn |= B8; nptn |= B8;  break; 
			default:  n = k;                  nptn |= B15; break; 
			} 
			--n;
		    }
		    while ( j < lgt2 ) {
			iptn <<= B2; lptn <<= B4; nptn <<= B4; 
			switch ( ca[j] ) {
			case 'A':             lptn |= B1; nptn |= B1;  break;   case 'C': iptn |= B1; lptn |= B2; nptn |= B2;  break; 
			case 'G': iptn |= B2; lptn |= B4; nptn |= B4;  break;   case 'T': iptn |= B3; lptn |= B8; nptn |= B8;  break; 
			default:  n = k;                  nptn |= B15; break; 
			} 
			--n;
			++j;
			
			match = false;
			if ( n < B0 ) { //## NOTE: no degenerated base in the current k-mer
			    iptn &= imsk; match = ( (bk2[iptn >> 6] & (1L << iptn)) != 0L );  //## NOTE: BitSet.get( iptn )
			}
			else {          //## NOTE: at least one degenerated base in the current k-mer
			    x = Arrays.binarySearch(lk2, (lptn &= lmsk)); x = -(++x); --x;
			    y = Arrays.binarySearch(lk2, (nptn &= lmsk)); y = -(++y); 
			    while ( ++x < y  &&  ((lkmer=lk2[x]) != (lkmer |= lptn)) ) {}
			    match = ( x != y );
			}
			
			cov[++i] += s;
			if ( match ) { ++cov[i]; --cov[j]; noTrim = false; }
			else if ( cov[i] < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		    while ( ++i < lgt2 ) {
			if ( (cov[i]+=s) < B0 ) cov[i] = B0;
			s = cov[i];
		    }
		}

		if ( ! noTrim ) {
		    //## trimming/clipping 3' R2  ######################################################
		    i = lgt2; x = 0;              //## NOTE: x >= 0 when suffix [i,lgt1[ contains > 50% troublesome bases
		    while ( --i >= 0 && bad[i] == B0 && cov[i] == B0 ) --x;
		    if ( i >= 0 ) {
			e2 = ( ++x >= 0 ) ? i : e2; s = m;  //## NOTE: s = 0 when m successive non-troublesome bases in suffix [i,lgt1[
			while ( --i >= 0 && s > B0 ) if ( bad[i] > B0 || cov[i] > B0 ) { e2 = ( ++x >= 0 ) ? i : e2; s = m; } else { --x; --s; }
		    }
		    //## trimming/clipping 5' R2  ######################################################
		    s2 = i = B_1; x = 0;
		    while ( ++i < e2 && bad[i] == B0 && cov[i] == B0 ) --x;
		    if ( i < e2 ) {
			s2 = ( ++x >= 0 ) ? i : s2; s = m;
			while ( ++i < e2 && s > B0 ) if ( bad[i] > B0 || cov[i] > B0 ) { s2 = ( ++x >= 0 ) ? i : s2; s = m; } else { --x; --s; }
		    }
		    ++s2;
		    //## assessing R2 length  ##########################################################
		    l = e2 - s2;                  //## NOTE: l = length of trimmed read
		    discard2 = ( l < minLgt );
		    //## assessing R2 composition  #####################################################
		    if ( ! discard2 ) {
			i = e2; x = 0; while ( --i >= s2 ) x += bad[i];
			discard2 = ( 100 * x > l * prop );  //## NOTE: x/l > prop/100, where x is the no. 'bad' bases in the trimmed read
		    }
		
		    if ( verbose && l != lgt2 ) display(l2_1, l2_2, l2_4, cov, bad, lgt2, s2, e2, discard2);
		}
	    }

	    //## writing paired-end outfiles  ######################################################
	    if ( discard1 && discard2 ) continue;
	    if ( discard1 ) {
		l = e2 - s2;
  		++cnt_r_outS; cnt_b_outS += l;		
		outS.write(l2_1);                                            outS.newLine();
		outS.write( ((l != lgt2) ? l2_2.substring(s2, e2) : l2_2) ); outS.newLine();
		outS.write(l2_3);                                            outS.newLine();
		outS.write( ((l != lgt2) ? l2_4.substring(s2, e2) : l2_4) ); outS.newLine();
		continue;
	    }
	    if ( discard2 ) {
		l = e1 - s1;
  		++cnt_r_outS; cnt_b_outS += l;		
		outS.write(l1_1);                                            outS.newLine();
		outS.write( ((l != lgt1) ? l1_2.substring(s1, e1) : l1_2) ); outS.newLine();
		outS.write(l1_3);                                            outS.newLine();
		outS.write( ((l != lgt1) ? l1_4.substring(s1, e1) : l1_4) ); outS.newLine();
		continue;
	    }
	    l = e1 - s1;
	    ++cnt_r_out1; cnt_b_out1 += l;
	    out1.write(l1_1);                                            out1.newLine();
	    out1.write( ((l != lgt1) ? l1_2.substring(s1, e1) : l1_2) ); out1.newLine();
	    out1.write(l1_3);                                            out1.newLine();
	    out1.write(((l != lgt1) ? l1_4.substring(s1, e1) : l1_4)  ); out1.newLine();
	    l = e2 - s2;
	    ++cnt_r_out2; cnt_b_out2 += l;
	    out2.write(l2_1);                                            out2.newLine();
	    out2.write( ((l != lgt2) ? l2_2.substring(s2, e2) : l2_2) ); out2.newLine();
	    out2.write(l2_3);                                            out2.newLine();
	    out2.write( ((l != lgt2) ? l2_4.substring(s2, e2) : l2_4) ); out2.newLine();
	}

	in1.close(); out1.close();
	if ( paired ) { in2.close(); out2.close(); outS.close(); }

	//#############################################################################################################
	//### displaying final stats                                                                                ###
	//#############################################################################################################
	System.out.println("");
	if ( ! paired ) {
	    x = String.format(Locale.US,"%,d",cnt_r_in1).length(); y = String.format(Locale.US,"%,d",cnt_b_in1).length(); 
	    System.out.println(String.format(Locale.US, "infile:             %,"+x+"d reads   %,"+y+"d bases", cnt_r_in1,  cnt_b_in1));
	    System.out.println(String.format(Locale.US, "outfile:            %,"+x+"d reads   %,"+y+"d bases", cnt_r_out1, cnt_b_out1));
	}
	else {
	    x = String.format(Locale.US,"%,d",cnt_r_in1).length(); y = String.format(Locale.US,"%,d",cnt_b_in1).length(); 
	    System.out.println(String.format(Locale.US, "infiles:            %,"+x+"d R1 reads   %,"+y+"d R1 bases", cnt_r_in1,  cnt_b_in1));
	    System.out.println(String.format(Locale.US, "                    %,"+x+"d R2 reads   %,"+y+"d R2 bases", cnt_r_in2,  cnt_b_in2));
	    System.out.println(String.format(Locale.US, "outfiles:           %,"+x+"d R1 reads   %,"+y+"d R1 bases", cnt_r_out1, cnt_b_out1));
	    System.out.println(String.format(Locale.US, "                    %,"+x+"d R2 reads   %,"+y+"d R2 bases", cnt_r_out2, cnt_b_out2));
	    System.out.println(String.format(Locale.US, "                    %,"+x+"d RS reads   %,"+y+"d RS bases", cnt_r_outS, cnt_b_outS));
	}
	t = (System.currentTimeMillis()-t) / 1000;
	System.out.println("");
	System.out.println("Total running time: " + (t/60) + "min" + (((t%=60)<10)?" 0":" ") + t + "sec");
	System.out.println("");

    }
  

    //## returns an ArrayList containing every alien sequence (and their reverse complement) written in the specified file
    static ArrayList<String> getAlienOligo (final File infile) throws IOException {
	ArrayList<String> oligo = new ArrayList<String>();
	//## reading file
	BufferedReader in = Files.newBufferedReader(Path.of(infile.toString()));
	String line;
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( (line.length() == 0) || line.startsWith("#") || line.startsWith("%")  || line.startsWith(">")) continue;
	    oligo.add(line.toUpperCase());
	}
	//## dealing with degenerated nucleotides
	StringBuilder sb; int b, i = -1;
	while ( ++i < oligo.size() ) {
	    b = (sb=new StringBuilder(oligo.get(i))).length();
	    while ( --b >= 0 ) {
		switch ( sb.charAt(b) ) {
		case 'A': case 'C': case 'G': case 'T': continue;
		case 'U': sb.setCharAt(b, 'T'); oligo.set(i, sb.toString()); continue;
		case 'M': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'C'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'R': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'W': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'S': oligo.remove(i); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'Y': oligo.remove(i); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'K': oligo.remove(i); sb.setCharAt(b, 'G'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString());                                                 --i; b = 0; continue;
		case 'B': oligo.remove(i); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString()); --i; b = 0; continue;
		case 'D': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString()); --i; b = 0; continue;
		case 'H': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString()); --i; b = 0; continue;
		case 'V': oligo.remove(i); sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString()); --i; b = 0; continue;
		case 'N':
		case 'X':
		    oligo.remove(i);
		    sb.setCharAt(b, 'A'); oligo.add(sb.toString()); sb.setCharAt(b, 'C'); oligo.add(sb.toString()); sb.setCharAt(b, 'G'); oligo.add(sb.toString()); sb.setCharAt(b, 'T'); oligo.add(sb.toString()); 
		    --i; b = 0; continue;
		default: System.err.println("the following alien sequence contains at least one incorrect nucleotide (" + sb.charAt(b) + ") : " + sb.toString()); System.exit(1);
		}
	    }
	}
	//## reverse-complementing
	i = oligo.size();
	while ( --i >= 0 ) {
	    sb = new StringBuilder((b=(line=oligo.get(i)).length()));
	    while ( --b >= 0 ) switch ( line.charAt(b) ) { case 'A': sb = sb.append('T'); continue; case 'C': sb = sb.append('G'); continue; case 'G': sb = sb.append('C'); continue; case 'T': sb = sb.append('A'); continue; }
	    oligo.add(sb.toString());
	}
	return oligo;
    }

    
    //## displays the trimming results in verbose mode
    static void display(final String lid, final String lread, final String lphred, final byte[] cov, final byte[] bad, final int lgt, final int start, final int end, final boolean discarded) {
	System.out.println("");
	System.out.println(lid + ((discarded)?"   [DISCARDED]":""));
	int o = -1; while ( ++o < lgt ) System.out.print( ((cov[o]>B0)?'=':' ') ) ; System.out.println("");
	System.out.println(lread);
	o = -1;     while ( ++o < lgt ) System.out.print( ((bad[o]>B0)?'*':' ') ) ; System.out.println("");
	//System.out.println(lphred);
	o = -1; while ( ++o < start ) System.out.print('>'); --o; while ( ++o < end ) System.out.print(' '); --o; while ( ++o < lgt ) System.out.print('<');
	System.out.println("");
    }
}


